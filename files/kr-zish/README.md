# kr-zish

A fork of Zhishen Wen's simple fun theme =)

![kr-zish](./kr-zish_preview.jpeg)

## Left prompt
User, host, abbreviated path, and git branch info

## Right prompt
Exit code and time

Enjoy!
